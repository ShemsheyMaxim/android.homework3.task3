package com.maxim.task3;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    Button clickMe;

    private final long doublePressInterval = 500;
    private boolean isWaitFirstClick = false;
    private int clickNumber = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        clickMe = (Button) findViewById(R.id.button_click_me);
        clickMe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (!isWaitFirstClick) {
                    isWaitFirstClick = true;
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            switch (clickNumber) {
                                case 1: {
                                    Intent intent = new Intent(MainActivity.this, Main2Activity.class);
                                    startActivity(intent);
                                    break;
                                }
                                case 2: {
                                    Intent intent = new Intent(MainActivity.this, Main3Activity.class);
                                    startActivity(intent);
                                    break;
                                }
                                case 3: {
                                    Intent intent = new Intent(MainActivity.this, Main4Activity.class);
                                    startActivity(intent);
                                    break;
                                }

                                case 4: {
                                    Toast.makeText(getApplicationContext(), "Try again! You can click only 1 or 2 or 3 times", Toast.LENGTH_SHORT).show();
                                    break;
                                }
                            }
                            clickNumber = 0;
                            isWaitFirstClick = false;
                        }
                    }, doublePressInterval);
                }
                clickNumber++;
            }
        });
    }
}